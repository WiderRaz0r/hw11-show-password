const regPasswordForm = document.querySelector('.password-form');
const submitButton = document.querySelector('.btn');
const password = document.querySelector(`.password`);
const confirmPassword = document.querySelector(`.confirm-password`);

const checkPassword = function (e) {
    e.preventDefault();
    if(password.value === ``){
        return document.querySelector(`.error`).innerText = `Введіть пароль`;
    } else if(password.value === confirmPassword.value){
        return alert('Вітаємо!')
    } else {
        document.querySelector(`.error`).innerText = `Паролі не співпадають! Введіть однакові значення.`;
    }
}

submitButton.addEventListener(`click`, checkPassword);

regPasswordForm.addEventListener('click', (e) =>{
    if(e.target.classList.contains('fa-eye')){
        e.target.classList.replace('fa-eye', 'fa-eye-slash');
        e.target.previousElementSibling.setAttribute('type', 'text')
    } else {
        e.target.classList.replace('fa-eye-slash', 'fa-eye');
        e.target.previousElementSibling.setAttribute('type', 'password')
    }
})

